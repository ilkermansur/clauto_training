# Data Types

* Text Type       :	str
* Numeric Types   :	int, float, complex
* Sequence Types  :	list, tuple, range
* Mapping Type    :	dict
* Set Types       :	set, frozenset
* Boolean Type    :	bool
* Binary Types    :	bytes, bytearray, memoryview
* None Type       :	NoneType

## String

String is a character arrays. Strings in python are surrounded by either single quotation marks, or double quotation marks.

```py
strVar = 'Hello'
strVar2 = 'Hello'

print (strVar) # strVar is a variable. Variables are like a container.   

print (id(strVar)) # refer same id with strVar2
print (id(strVar2))
```

You can use multiline string.

```py
mlStr = '''Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua'''
```

Square brackets can be used to access elements of the string.

```py
strVar = 'string'

print (strVar[0])     # is the first element of string
print (strVar[-1])     # is the last element of string
print (strVar[1])     # is the second element of string
print (strVar[:3])    # from the begining to third element
print (strVar[2:])    # from the second element to end
print (strVar[:])     # from begining to end

```
you can count the element of string.

```py

strVar = 'string'
print (len(strVar))

```
check the word in string

```py

text = 'If you can not explain something siply, you dont understand it'

print ('explain' in text) # returns 'True'
print ('explain' not in text) # returns 'False'
print ('ExplaiN' in text) # returns 'False'
print ('ExplaiN' in text) # returns 'True'

```
To change word, there are some methed:

* upper
* lower
* title
* strip
* replace
* split
* find

```py
text = '    CaLL ManaGer    '

print (text.upper()) # returns CALL MANAGER
print (text.lower()) # returns call manager
print (text.title()) # returns Call Manager
print (text.strip()) # strip, removes whitespaces from begining or ending
print (text.replace('    ','****')) # returns ****Call ManaGer****
print (text.split()) # returns ['', '', '', '', 'CaLL', 'ManaGer', '', '', '', '']
print (text.split('    ')) # returns ['', 'CaLL ManaGer', '']
print (text.split('Mana')) # return ['    CaLL ', 'Ger    ']
print (text.find('Mana'))  # returns  9 (index of begining of the keyword)

```
**Example**

Lets split a string

```py
config = 'interface GigabitEthernet0/0/1'

print(config.split())           # returns ['interface', 'GigabitEthernet0/0/1']
print(config.split()[0])        # interface
print(config.split()[1])        # GigabitEthernet0/0/1

interface = config.split()[1]
print (interface)               # GigabitEthernet0/0/1

```

String Concatenation

```py

strVar1 = '11'
strVar2 = '22'

strTotal = strVar1 + strVar2
print (strTotal)                         # returns 1122

strTotal = strVar1 * 10
print (strTotal)                         # returns 11111111111111111111

strTotal = int(strVar1) * int(strVar2)
print (strTotal)                         # returns 242

```
**String Format**

The format() method takes the passed arguments, formats them, and places them in the string where the placeholders {} are:

```py
config = 'interface GigabitEthernet{}'.format('0/0/1')
print (config # interface GigabitEthernet0/0/1

ipAddr = 'ip address {} {}'.format('10.0.0.1','255.255.255.0')
print (ipAddr) # ip address 10.0.0.1 255.255.255.0

ipAddr = 'ip address {1} {0}'.format('10.0.0.1','255.255.255.0')
print (ipAddr) # ip address 255.255.255.0 10.0.0.1

ipAddr = '10.0.0.1'
mask = '255.255.255.0'

ipAddress = 'ip address {} {}'

print (ipAddress.format(ipAddr,mask)) # ip address 10.0.0.1 255.255.255.0

```

**PS:** You can use F string

```py

ipAddr = '10.0.0.1'
mask = '255.255.255.0'

ipAddress = f'ip address {ipAddr} {mask}'
print (ipAddress) # ip address 10.0.0.1 255.255.255.0
```
**String Escape Character**


```py
config = 'interface GigabitEthernet0/0/1\nip address 10.0.0.1 255.255.255.0'

print (config) # interface GigabitEthernet0/0/1
               # ip address 10.0.0.1 255.255.255.0

ipAddr = 'Call\tManager'
print (ipAddr) # Call    Manager

```

## Numbers









